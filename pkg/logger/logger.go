package logger

import (
	"golang-darknet/utils"
	"time"

	"github.com/gofiber/fiber/v2"
)

type ILogger interface {
	Print() ILogger
	SetResponse(res any)
}

type logger struct {
	Time       string `json:"time"`
	Ip         string `json:"ip"`
	Method     string `json:"method"`
	StatusCode int    `json:"status_code"`
	Path       string `json:"path"`
	Response   any    `json:"response"`
}

func (l *logger) Print() ILogger {
	utils.Debug(l)
	return l
}

func (l *logger) SetResponse(res any) {
	l.Response = res
}

func NewLogger(c *fiber.Ctx, res any) ILogger {
	log := &logger{
		Time:       time.Now().Local().Format("2006-01-02 15:04:05"),
		Ip:         c.IP(),
		Method:     c.Method(),
		Path:       c.Path(),
		StatusCode: c.Response().StatusCode(),
	}

	log.SetResponse(res)
	return log
}
