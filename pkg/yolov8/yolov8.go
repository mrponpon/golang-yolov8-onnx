package yolov8

import (
	"fmt"
	"golang-darknet/config"
	"golang-darknet/models"
	"golang-darknet/utils"
	"image"
	"sort"
	"sync"

	"github.com/nfnt/resize"
	ort "github.com/yalue/onnxruntime_go"
)

type IYolov8 interface {
	LoadModel() error
	Predict(img image.Image) *map[string][]models.Result
	GetSession() *ort.DynamicSession[float32, float32]
}

type yolov8 struct {
	cfg     config.IAIconfig
	session *ort.DynamicSession[float32, float32]
}

func InitYolov8(cfg config.IAIconfig) IYolov8 {
	return &yolov8{
		cfg: cfg,
	}
}

func (y *yolov8) LoadModel() error {
	ort.SetSharedLibraryPath(y.cfg.Config())
	ort.InitializeEnvironment()
	session, err := ort.NewDynamicSession[float32, float32](y.cfg.Weight(), []string{"images"}, []string{"output0"})
	if err != nil {
		return err
	}
	y.session = session
	return nil
}

func appendColor(wg *sync.WaitGroup, r uint32, g uint32, b uint32, red *[]float32, green *[]float32, blue *[]float32) {
	defer wg.Done()
	*red = append(*red, float32(r/257)/255.0)
	*green = append(*green, float32(g/257)/255.0)
	*blue = append(*blue, float32(b/257)/255.0)
}

//	func appendColor(r uint32, g uint32, b uint32, red *[]float32, green *[]float32, blue *[]float32) {
//		*red = append(*red, float32(r/257)/255.0)
//		*green = append(*green, float32(g/257)/255.0)
//		*blue = append(*blue, float32(b/257)/255.0)
//	}
func (y *yolov8) Predict(img image.Image) *map[string][]models.Result {
	wg := new(sync.WaitGroup)

	size := img.Bounds().Size()
	img_width, img_height := int64(size.X), int64(size.Y)
	img = resize.Resize(640, 640, img, resize.Lanczos3)
	red := []float32{}
	green := []float32{}
	blue := []float32{}
	for y := 0; y < 640; y++ {
		for x := 0; x < 640; x++ {
			wg.Add(1)
			r, g, b, _ := img.At(x, y).RGBA()
			appendColor(wg, r, g, b, &red, &green, &blue)
		}
	}
	wg.Wait()
	// for y := 0; y < 640; y++ {
	// 	for x := 0; x < 640; x++ {
	// 		r, g, b, _ := img.At(x, y).RGBA()
	// 		appendColor(r, g, b, &red, &green, &blue)
	// 	}
	// }
	// wg.Wait()
	input := append(red, green...)
	input = append(input, blue...)
	inputShape := ort.NewShape(1, 3, 640, 640)
	inputTensor, _ := ort.NewTensor(inputShape, input)
	outputShape := ort.NewShape(1, 84, 8400)
	outputTensor, _ := ort.NewEmptyTensor[float32](outputShape)
	y.session.Run([]*ort.Tensor[float32]{inputTensor}, []*ort.Tensor[float32]{outputTensor})
	output := outputTensor.GetData()
	yolo_classes := y.cfg.Classname()
	boxes := [][]interface{}{}
	for index := 0; index < 8400; index++ {
		class_id, prob := 0, float32(0.0)
		for col := 0; col < 80; col++ {
			if output[8400*(col+4)+index] > prob {
				prob = output[8400*(col+4)+index]
				class_id = col
			}
		}
		if prob < 0.5 {
			continue
		}
		label := yolo_classes[class_id]
		xc := output[index]
		yc := output[8400+index]
		w := output[2*8400+index]
		h := output[3*8400+index]
		x1 := (xc - w/2) / 640 * float32(img_width)
		y1 := (yc - h/2) / 640 * float32(img_height)
		x2 := (xc + w/2) / 640 * float32(img_width)
		y2 := (yc + h/2) / 640 * float32(img_height)
		boxes = append(boxes, []interface{}{float64(x1), float64(y1), float64(x2), float64(y2), label, prob})
	}

	sort.Slice(boxes, func(i, j int) bool {
		return boxes[i][5].(float32) > boxes[j][5].(float32)
	})
	var result = make(map[string][]models.Result)
	for len(boxes) > 0 {
		xmin := boxes[0][0].(float64)
		ymin := boxes[0][1].(float64)
		xmax := boxes[0][2].(float64)
		ymax := boxes[0][3].(float64)
		label_string := fmt.Sprintf("%v", boxes[0][4])
		conf := boxes[0][5].(float32)
		result[label_string] = append(result[label_string],
			models.Result{
				Probability: conf,
				Xmin:        xmin,
				Ymin:        ymin,
				Xmax:        xmax,
				Ymax:        ymax,
			})
		tmp := [][]interface{}{}
		for _, box := range boxes {
			if utils.IOU(boxes[0], box) < 0.7 {
				tmp = append(tmp, box)
			}
		}
		boxes = tmp
	}

	return &result
}

func (y *yolov8) GetSession() *ort.DynamicSession[float32, float32] {
	return y.session
}
