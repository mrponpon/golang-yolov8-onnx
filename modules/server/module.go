package server

import (
	predicthandler "golang-darknet/modules/predict/predictHandler"
	predictrepository "golang-darknet/modules/predict/predictRepository"
	predictusecase "golang-darknet/modules/predict/predictUsecase"

	"github.com/gofiber/fiber/v2"
)

type IModuleFactory interface {
	TodoModule()
}

type moduleFactory struct {
	r fiber.Router
	s *server
}

func InitModule(r fiber.Router, s *server) IModuleFactory {
	return &moduleFactory{
		r: r,
		s: s,
	}
}

func (m *moduleFactory) TodoModule() {
	repository := predictrepository.PredictRepository(m.s.cfg, m.s.ai)
	usecase := predictusecase.PredictUsecase(m.s.cfg, repository)
	handler := predicthandler.PredictHandler(m.s.cfg, usecase)
	router := m.r.Group("/ai")
	router.Post("/prediction", handler.Prediction)
}
