package predictusecase

import (
	"golang-darknet/config"
	"golang-darknet/models"
	predictrepository "golang-darknet/modules/predict/predictRepository"
)

type IPredictusecase interface {
	Prediction(base64 *models.Base64str) (*map[string][]models.Result, error)
}

type predictusecase struct {
	cfg               config.IConfig
	predictrepository predictrepository.IPredictrepository
}

func PredictUsecase(cfg config.IConfig, predictrepository predictrepository.IPredictrepository) IPredictusecase {
	return &predictusecase{
		cfg:               cfg,
		predictrepository: predictrepository,
	}
}

func (u *predictusecase) Prediction(base64 *models.Base64str) (*map[string][]models.Result, error) {
	predict_result, err := u.predictrepository.Prediction(base64)
	if err != nil {
		return nil, err
	}
	return predict_result, nil
}
