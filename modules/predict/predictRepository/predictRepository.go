package predictrepository

import (
	"golang-darknet/config"
	"golang-darknet/models"
	"golang-darknet/pkg/yolov8"
	"golang-darknet/utils"
)

type IPredictrepository interface {
	Prediction(base64 *models.Base64str) (*map[string][]models.Result, error)
}

type predictrepository struct {
	cfg config.IConfig
	ai  yolov8.IYolov8
}

func PredictRepository(cfg config.IConfig, ai yolov8.IYolov8) IPredictrepository {
	return &predictrepository{
		cfg: cfg,
		ai:  ai,
	}
}

func (t *predictrepository) Prediction(base64 *models.Base64str) (*map[string][]models.Result, error) {
	img := utils.Base64toimg(base64.Base64)
	predict_result := t.ai.Predict(img)
	return predict_result, nil
}
