package predicthandler

import (
	"golang-darknet/config"
	"golang-darknet/entities"
	"golang-darknet/models"
	predictusecase "golang-darknet/modules/predict/predictUsecase"

	"github.com/gofiber/fiber/v2"
)

type IPredicthandler interface {
	Prediction(c *fiber.Ctx) error
}

type predicthandler struct {
	cfg            config.IConfig
	predictusecase predictusecase.IPredictusecase
}

func PredictHandler(cfg config.IConfig, predictusecase predictusecase.IPredictusecase) IPredicthandler {
	return &predicthandler{
		cfg:            cfg,
		predictusecase: predictusecase,
	}
}

func (h *predicthandler) Prediction(c *fiber.Ctx) error {
	req := new(models.Base64str)
	err := c.BodyParser(req)
	if err != nil {
		return entities.InitResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}
	result, err := h.predictusecase.Prediction(req)
	if err != nil {
		return entities.InitResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}
	return entities.InitResponse(c).Success(fiber.StatusOK, result).Res()

}
