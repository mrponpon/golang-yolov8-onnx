package main

import (
	"fmt"
	"golang-darknet/config"
	"golang-darknet/modules/server"
	"golang-darknet/pkg/yolov8"
	"os"
)

func envPath() string {
	if len(os.Args) == 1 {
		return ".env"
	} else {
		return os.Args[1]
	}
}

func main() {
	cfg := config.LoadConfig(envPath())
	ai := yolov8.InitYolov8(cfg.AI())
	err := ai.LoadModel()
	if err != nil {
		fmt.Println("Load Model Error")
	} else {
		fmt.Println("Load Model Success")
	}
	server.InitServer(cfg, ai).Start()
}
