package entities

import (
	"golang-darknet/pkg/logger"

	"github.com/gofiber/fiber/v2"
)

type IResponse interface {
	Success(code int, data any) IResponse
	Error(code int, msg string) IResponse
	Res() error
}

type response struct {
	StatusCode int
	Data       any
	ErrorRes   *ErrorResponse
	Context    *fiber.Ctx
	IsError    bool
}

type ErrorResponse struct {
	Msg string `json:"message"`
}

func InitResponse(c *fiber.Ctx) IResponse {
	return &response{
		Context: c,
	}
}

func (r *response) Success(code int, data any) IResponse {
	r.StatusCode = code
	r.Data = data
	logger.NewLogger(r.Context, &r.Data).Print()

	return r
}

func (r *response) Error(code int, msg string) IResponse {
	r.StatusCode = code
	r.ErrorRes = &ErrorResponse{
		Msg: msg,
	}
	r.IsError = true
	logger.NewLogger(r.Context, &r.ErrorRes).Print()

	return r
}

func (r *response) Res() error {
	return r.Context.Status(r.StatusCode).JSON(func() any {
		if r.IsError {
			return &r.ErrorRes
		}
		return &r.Data
	}())
}
