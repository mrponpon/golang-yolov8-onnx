package utils

import (
	"encoding/base64"
	"fmt"
	"image"
	"image/jpeg"
	"strings"
)

func Base64toimg(base64str string) image.Image {
	reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(base64str))
	src, err := jpeg.Decode(reader)
	if err != nil {
		fmt.Printf("decode base64 error:%v", err)

		return nil
	}
	return src
}
