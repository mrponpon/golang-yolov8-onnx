package models

type Result struct {
	Probability float32 `json:"prob"`
	Xmin        float64 `json:"xmin"`
	Ymin        float64 `json:"ymin"`
	Xmax        float64 `json:"xmax"`
	Ymax        float64 `json:"ymax"`
}

type Base64str struct {
	Base64 string `form:"base64"`
}
